var Relogio = {
    init: function () {
        this.date = new Date($('#hora').data('init-date'));

        var self = this;

        setInterval(function(){
            self.date.setSeconds(self.date.getSeconds() + 1);
            self.move(self.date);
        }, 1000);

    },
    
    move:function(datetime){
        var self = this;
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        
        var momentoAtual = datetime;
        var hora = addZero(momentoAtual.getHours());
        var minuto = addZero(momentoAtual.getMinutes());
        var segundo = addZero(momentoAtual.getSeconds());

        var horaImprimivel = hora + ":" + minuto;
        var current = $('#hora').html();
        //comparação para não gerar reflow e otimizar desempenho do browser
        if(current != horaImprimivel){
            $('#hora').html(horaImprimivel);    
        }
        
    }


};


$(document).ready(function () {
    Relogio.init();

});
