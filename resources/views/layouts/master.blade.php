<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>COMERC - TESTE - Mercado Imobiliário</title>

        <link href="{{ url('/assets/css/main.min.css')}}" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <!--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">-->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        @yield('extra-head')

    </head>
    <body>

        @yield('header')
        @yield('content')
        @yield('footer')

        <?php
        /**
         * 
         * <div class="flex-center position-ref full-height d-none">
          @if (Route::has('login'))
          <div class="top-right links">
          @auth
          <a href="{{ url('/home') }}">Home</a>
          @else
          <a href="{{ route('login') }}">Login</a>
          <a href="{{ route('register') }}">Register</a>
          @endauth
          </div>
          @endif

          <div class="content">
          <div class="title m-b-md">
          Laravel
          </div>

          <div class="links">
          <a href="https://laravel.com/docs">Documentation</a>
          <a href="https://laracasts.com">Laracasts</a>
          <a href="https://laravel-news.com">News</a>
          <a href="https://forge.laravel.com">Forge</a>
          <a href="https://github.com/laravel/laravel">GitHub</a>
          </div>
          </div>
          </div>
         */
        ?>
        <div class="bootbox modal fade show" tabindex="-1" role="dialog" id='modal-loading'>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bootbox-body">
                            <div class="text-center"><i class="fa fa-spin fa-spinner"></i> Carregando...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src='/assets/js/main.min.js'></script>
        <script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        </script>

    </body>
</html>
