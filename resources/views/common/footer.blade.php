<footer class='footer col-12 mt-2'>
    <div class='row'>
        <div class="footer__left col-sm-3 col-12 text-sm-left text-center text-sm-left ">
            <span class="footer__left--hora" id='hora' data-init-date='{{date('Y-m-d H:i:s') }}'>
                {{ date('H:i') }}
            </span> 
            <span class="footer__left--previsao">São Paulo {{ $clima['temp_c'] ?? ''}} &deg;</span>
        </div>
        <div class="footer__right col-sm-9 col-12 text-sm-right text-center ">
            <img src='/assets/images/footer-logo.png' alt='' />
        </div>
    </div>
</footer>