<form method="POST" action="{{ url('/numeros/save') }}" class="form-horizontal">
        {{ csrf_field() }}

    <input type='hidden' name='numero_id' value='{{ isset($numero) ? $numero->numero_id : ''}}' />
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                @if(isset($numero))
                    Editar ({{$numero->numero_id}})
                @else
                    Novo Número
                @endif
            </div>
            
        </div>
        <div class="portlet-body">
            <div class="form-group">
                <label for="titulo">Título</label>
                <input type="text" class="form-control" id="titulo" placeholder="Título" name='titulo' value='{{isset($numero) ? $numero->titulo : ''}}'>
            </div>
            
            <div class="form-group">
                <label for="valor">Valor</label>
                <input type="number" class="form-control" id="valor" placeholder="Valor" step='1' min='0' pattern="\d*" name='valor' value='{{isset($numero) ? $numero->valor : ''}}'>
            </div>
            
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control">
                    <option value="0" {{ isset($numero) && $numero->status == 0 ? 'selected="selected"' : ''}} >Não</option>
                    <option value="1" {{ isset($numero) && $numero->status == 1 ? 'selected="selected"' : ''}}>Sim</option>
                </select>
            </div>
            <div class="form-check">
                <label for="titulo">Data/hora</label>
                <input type="datetime-local" class="form-control" id="data_hora" name="data_hora" placeholder="Data/Hora" value='{{ isset($numero) ? date('Y-m-d\TH:i',strtotime($numero->data_hora)) : '' }}'>
            </div>
            
        </div>
        
        <div class="portlet-footer">
            <div class='actions text-right'>
                <button type="submit" class="btn btn-primary btn-success">Salvar</button>
            </div>
        </div>
    </div>



    
</form>