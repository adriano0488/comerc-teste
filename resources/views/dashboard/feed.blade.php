<div class='main__feed--content'>
    @if($feed)
    <h1 class='main__feed--title'>
        {{$feed['title']}}
    </h1>
    
    <div class='main__feed--description'>
        {!!$feed['description']!!}
    </div>
    
    @endif
</div>