<header class='header col-12'>
    <div class='row'>
        <div class='col-3 header__left'>
            <div class='header__left--logo'>
                <a href='#' title='Commerc Teste'>
                    <img src='/assets/images/logo.png' alt='' class='img-responsive' />
                </a>
            </div>
        </div>
        <div class='col-9 header__right'>
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href='/logout' class="btn btn-dark-color float-none float-sm-right mt-4 ">
                            Logout
                        </a>
                        <a href='/numeros' class="btn btn-dark-color float-none float-sm-right mt-4 mr-2">
                            Números
                        </a>
                    @else
                        <a href='/register' class="btn btn-dark-color float-none float-sm-right mt-4 ">
                            Registrar
                        </a>
                    @endauth
                </div>
            @endif
            
        </div>
    </div>  
</header>

