<?php

namespace App\Http\Helpers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Yahoo
 *
 * @author Adriano
 */
class Feed {
    
    /**
     * 
     * Efetuar consulta no RSS do google e retornar 
     * @return array
     */

    public static function getFeed(): array {
        
        $url = 'https://news.google.com/news/rss/search/section/q/mercado%20imobiliario/mercado%20imobiliario?hl=en&gl=US&ned=br';
        $internalErrors = libxml_use_internal_errors(true);

        $feed = new \SimpleXMLElement($url, null, true);
        $itens = $feed->channel->item;
        
        if(count($itens) > 0){
            $result = [];
            foreach($itens as $item){
                
                $description = (string)$item->description;
                $dom = new \DOMDocument();
                $dom->loadHTML($description);
                $img= $dom->getElementsByTagName('img');
                if($img->length > 0){
                    
                    
                    $img_url = $img->item(0)->getAttribute('src');
                    $escape_url = str_replace('/','\/',$img_url);
                    #preg_match_all("/(.*){$escape_url}(.*)>/im", $description,$results);
                    #dd($results);
                    $item->img_url = $img_url;
                }else{
                    $item->img_url = '';
                }
                
                
                $result[] = (array) $item;
                
            }
            return $result[0];
        }
        
        return [];
        
        
    }

}
