@foreach($numeros_stats as $numero)
<div class='main__stats--block'>
    <h3>{{ $numero->titulo }}</h3>
    <div class='numero'>
        <span>{{number_format($numero->valor,0,',','.')}}</span>
    </div>
</div>

@endforeach