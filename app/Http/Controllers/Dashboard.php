<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Dashboard extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $numeros_stats = \App\Numero::where('status', 1)->orderBy('data_hora', 'desc')->get();
        $feed = \App\Http\Helpers\Feed::getFeed();

        return view('dashboard', [
            'feed' => $feed,
            'numeros_stats' => $numeros_stats,
        ]);
    }

}
