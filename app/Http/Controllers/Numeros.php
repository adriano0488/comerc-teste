<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class Numeros extends Controller
{
    public function index(){
        
        $numeros = \App\Numero::orderBy('numero_id','desc')->paginate(10);
        
        return view('admin',[
            'numeros'=>$numeros,
            
        ]);
        
    }
    
    
    /**
     * Atender requisição AJAX e retornar JSON ao remover registro
     * @return type
     */
    public function delete(){
        
        $id = Input::get('numero_id');
        $result = [];
        if(empty($id)){
            $result['success'] = false;
            $result['message'] = 'ID de registro não informado';
            return response()->json($result);
        }
        try{
            \App\Numero::where('numero_id',$id)->delete();    
        }catch(\Exception $e){
            $result['success'] = false;
            $result['message'] = 'Erro ao remover registro - '.$e->getMessage();
            return response()->json($result);
            
        }
        
        $result['success'] = true;
        $result['message'] = 'Registro removido com sucesso';
        return response()->json($result);
        
    }
    
    public function editar($id){
        
        $numero = \App\Numero::find($id);
        
        if(empty($numero)){
            return redirect()->to('/numeros');
        }
        
        return view('form_numero',[
            'numero'=>$numero,
            
        ]);
        
    }
    public function novo(){
        return view('form_numero',[
            
        ]);
        
    }
    public function save(){
        
        $id = Input::get('numero_id');
        $input = Input::all();
        if(empty($id)){
            $numero = new \App\Numero;
        }else{
            $numero = \App\Numero::find($id);
            if(empty($numero)){
                $numero = new \App\Numero;
            }
            
        }
        
        
        $params = [
            'titulo'=>trim(substr($input['titulo'],0,255)),
            'status'=>boolval($input['status']) ? 1 : 0,
            'valor'=>$input['valor'],
            'data_hora'=>!empty($data_hora) ? str_replace('T',' ',$input['data_hora']) : date('Y-m-d H:i:s'),
        ];
        
        foreach($params as $k=>$v){
            $numero->$k = $v;
        }
        
        
        try{
            $numero->save();
        }catch(\Exception $e){
            \Session::flash('error_message','Ocorreu um erro ao salvar os dados');
            return redirect()->back()->withInput();
        }
        
        \Session::flash('success_message','Registro efetuado com sucesso.');
        return redirect('/numeros');
        
        
        
        
    }
    
    
}
