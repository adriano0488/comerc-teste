@extends('layouts.master')

@section('content')
<main class="container-fluid main">
    <div class="row">
        <div class='col-3 main__stats'>
            @include('dashboard.stats')            
        </div>
        <div class="col-9 main__feed">
            @include('dashboard.feed')
        </div>
    </div>
</main>
@endsection


@section('header')
    @include('common.header')
@stop

@section('footer')
    @include('common.footer')
@stop