<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $clima = \App\Http\Helpers\Yahoo::getWeather();
        \View::share('clima',$clima);
        
        
        #dd($this->app);
        \Illuminate\Support\Facades\Session::put('teste','valor');

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
