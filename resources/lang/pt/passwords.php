<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha deve possuir ao menos 6 caracteres e confirmação.',
    'reset' => 'A senha foi resetada!',
    'sent' => 'Foi enviado um novo link para seu email!',
    'token' => 'Token inválido.',
    'user' => "Usuário não encontrado para este registro.",

];
