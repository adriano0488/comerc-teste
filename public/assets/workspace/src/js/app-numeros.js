
var Numeros = {
    
    deleteNumero:function(id) {
        var s = this;
        bootbox.confirm({
            "message": "Deseja realmente remover este registro?",
            "buttons": {
                "confirm": {
                    "label": 'Sim',
                    "className": 'btn-success'
                },
                "cancel": {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            "callback": function (result) {
                if(result == true){
                    s._deleteNumero(id);
                }
            }
        });
    },
    _deleteNumero: function (id) {
        $.ajax({
            url: '/numeros/delete',
            type:'POST',
            data: {
                "numero_id": id
            },
            beforeSend: function () {
                $('#modal-loading').modal('show');
            },
            success: function (result) {
                if (result.success == true) {
                    bootbox.alert({
                        size: "small",
                        message: "Registro apagado com sucesso",
                        callback: function () {
                            window.location.reload();
                        }
                    });

                    $('#modal-loading').modal('hide');
                }
            }
        }).fail(function () {
            $('#modal-loading').modal('hide');
            bootbox.alert('Ocorreu um erro, tente novamente');
        });

    }


};