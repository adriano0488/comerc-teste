<?php

namespace App\Http\Helpers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Yahoo
 *
 * @author Adriano
 */
class Yahoo {
    
    /**
     * 
     * Efetuar consultar de dados no serviço do yahoo para consultar de informações climáticas
     * @return array
     */

    public static function getWeather(): array {
        $cod_cidade = '455827'; //código de são paulo
        $params = [
            'q' => 'select * from weather.forecast where woeid in (' . $cod_cidade . ')',
            'format' => 'json',
            'lang' => 'pt-br'
        ];

        $url = 'https://query.yahooapis.com/v1/public/yql?';
        $url = $url . http_build_query($params);
        $result = file_get_contents($url);

        $result = json_decode($result, true);

        $umi = $result['query']['results']['channel']['atmosphere']['humidity'];
        $temp = $result['query']['results']['channel']['item']['condition']['temp'];
        $temp_c = intval( ($temp - 32) /1.8 ); //converter farenheit para celcius
        return [
            'temp_f'=>$temp,
            'temp_c'=>$temp_c,
            'umi'=>$umi,
            'location'=>$result['query']['results']['channel']['location']
        ];
    }

}
