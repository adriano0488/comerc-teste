<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Route::middleware('web')->get('/', function () {
#    return view('welcome');
#});

Auth::routes();

Route::get('/', 'Dashboard@index')->name('dashboard');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/numeros', 'Numeros@index')->name('numeros');
Route::get('/numeros/novo', 'Numeros@novo')->name('novo-numero');
Route::post('/numeros/delete', 'Numeros@delete')->name('delete-numeros');
Route::post('/numeros/save', 'Numeros@save')->name('save-numeros');
Route::get('/numeros/editar/{id}', 'Numeros@editar')->name('editar-numeros');
