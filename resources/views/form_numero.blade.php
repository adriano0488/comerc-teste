@extends('layouts.master')

@section('content')
<main class="container main">
    <div class='main__top'>
        <h1 class='main__title mb-3 float-none float-sm-left'>
            Gestão de Números
        </h1>
        <div class='float-none float-sm-right'>
            <a href='/' class='btn btn-dark-default mr-2'>
                Ver Dashboard
            </a>
            
        </div>
    </div>
    <div class='clearfix'></div>
    
    <div class="row">
        <div class='col-12 main__table'>
            @include('admin.form_numero')
        </div>
        
    </div>
</main>
@endsection


@section('header')
    @include('common.header')
@stop

@section('footer')
    @include('common.footer')
@stop


@section('extra-head')
<link rel="stylesheet" type='text/css' href='/assets/css/jquery.datetimepicker.min.css' />
@stop