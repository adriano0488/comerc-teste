# README #

Projeto de Teste Técnico para Comerc Energia

### Sobre ###

* Trata-se de um plataforma para exibição de números consolidados referentes a dados imobiliários sistema feito em laravel 5.5

### Tecnologias utilizadas ###
* PHP 7.1.9
* Laravel 5.5
* mysql
* GULP
* sass 3.5.3
* jquery 3.0
* bootstrap 4

### Instalação - Requisitos ###

* PHP >= 7.0.0
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

### VHOST (apache) ###

###### <VirtualHost *:80>
###### ServerName comerc.dev 
######	DocumentRoot "/var/www/www/comerc.dev/public"
######	<Directory  "/var/www/www/comerc.dev/public/">
######		Options +Indexes +Includes +FollowSymLinks +MultiViews
######		AllowOverride All
######		Require local
######	</Directory>
######/VirtualHost>

### Servidor laravel ###
######Utilizar comandos:
######
######$ cd /var/www/www/comerc.dev/
######$ composer update
######$ php artisan serve (Opcional, caso servidor WEB não esteja configurado)
######
######Via browser o projetos estará acessível em http://127.0.0.1
######
######Obs: atentar-se as pastas dentro de storage, devem ser criadas conforme estrutura do laravel e devem ter as devidas permissões de escrita.
######
### Inicialização de dados ###

######Tendo banco de dados mysql (ou qualquer outro configurado)
######copie o arquivo .env.example para .env e edite as configurações de conexão.

######DB_CONNECTION=mysql
######DB_HOST=127.0.0.1
######DB_PORT=3306
######DB_DATABASE=comerc_teste
######DB_USERNAME=root
######DB_PASSWORD=

######Em seguida rode os comandos:
######
######php artisan migrate
######php artisan db:seed
######
######Obs: as configurações acima são somente para que o projeto possa rodar
######
### Exemplo online ###
###### http://comerc-teste.unioferta.com.br
###### Usuário: teste@teste.com.br
###### Senha: 123456