<?php

use Illuminate\Database\Seeder;

class numeros extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


	$user = new User();
	$user->username = 'Teste';
	$user->password = Hash::make('123456');
	$user->email = 'teste@teste.com.br';
	$user->save();

        \DB::table('numeros')->insert([
            'numero_id' => 1,
            'titulo' => 'Imóveis Disponíveis',
            'status' => 1,
            'valor' => 32193,
            'data_hora' => date('Y-m-d H:i:s'),
        ]);
        
        
        \DB::table('numeros')->insert([
            'numero_id' => 2,
            'titulo' => 'Imóveis Alugados',
            'status' => 1,
            'valor' => 1203,
            'data_hora' => date('Y-m-d H:i:s'),
        ]);
        
        \DB::table('numeros')->insert([
            'numero_id' => 3,
            'titulo' => 'Imóveis Vendidos',
            'status' => 1,
            'valor' => 103,
            'data_hora' => date('Y-m-d H:i:s'),
        ]);
        
        
    }
}
