<table class="table table-striped table-responsive">
    <thead>
        <tr>
            <th>ID</th>
            <th>TÍTULO</th>
            <th>VALOR</th>
            <th>DATA/HORA</th>
            <th class='text-center text-sm-right'>AÇÕES</th>
        </tr>
    </thead>
    <tbody>
        @foreach($numeros as $numero)
        <tr>
            <th scope="row">{{$numero->numero_id}}</th>
            <td>{{$numero->titulo}}</td>
            <td>{{number_format($numero->valor,0,',','.')}}</td>
            <td>
                @if($numero->data_hora)
                    {{ date('d/m/Y H:i',strtotime($numero->data_hora)) }}
                @endif
            </td>
            <td class='text-center text-sm-right '>
                <a href='/numeros/editar/{{$numero->numero_id}}' class='btn btn-dark-default mr-2'>EDITAR [-]</a>
                <a href='javascript:void(0);' class='btn btn-dark-default' onclick='Numeros.deleteNumero({{$numero->numero_id}})'>EXCLUIR [x]</a>
            </td>
        </tr>
        
        @endforeach
        
    </tbody>
</table>


<nav class='ml-auto mr-auto text-center d-table mt-2'>
    {{ $numeros->render('vendor.pagination.bootstrap-4')}}
</nav>
